package com.company.databases;

import com.company.datatypes.Film;
import com.company.datatypes.FilmOrder;
import com.company.datatypes.User;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class FilmOrdersDatabase extends Database{
    protected UsersDatabase users_database;
    protected FilmsDatabase films_database;
    protected ArrayList<FilmOrder> filmorders_table;
    public FilmOrdersDatabase(String table_name, UsersDatabase users_database, FilmsDatabase films_database) {
        super(table_name);
        this.users_database = users_database;
        this.films_database = films_database;
        filmorders_table = new ArrayList<>();
        create_FilmOrders_table();
        import_data();
    }
    public ArrayList<FilmOrder> get_data(){
        return filmorders_table;
    }
    protected void create_FilmOrders_table(){
        try {
            String sql = """
                    CREATE TABLE IF NOT EXISTS  FilmOrders(
                      "ID" serial PRIMARY KEY,
                      "User_ID" INT  NOT NULL,
                      "Film_ID" INT  NOT NULL,
                      "Order_Date" DATE NOT NULL,
                      CONSTRAINT fk_user_id
                      	FOREIGN KEY("User_ID")\s
                      	REFERENCES Users("ID"),
                      CONSTRAINT fk_film_id
                      	FOREIGN KEY("Film_ID")\s
                      	REFERENCES Films("ID")
                    );""";
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public int index_of_film_order(FilmOrder film_order)
    {
        int film_order_index;
        OptionalInt optional_index = IntStream.range(0, filmorders_table.size())
                .filter(i -> (Objects.equals(filmorders_table.get(i).get_User_ID(),film_order.get_User_ID()))
                        &&(Objects.equals(filmorders_table.get(i).get_Film_ID(), film_order.get_Film_ID())))
                .findFirst();
        if(optional_index.isPresent()){
            film_order_index = optional_index.getAsInt();
            return film_order_index;
        }
        return -1;
    }

    public void add_film_order(FilmOrder film_order){
        filmorders_table.add(film_order);
        try {
            String sql = String.format("INSERT INTO FilmOrders(\"User_ID\", \"Film_ID\", \"Order_Date\")\n" +
                    "VALUES(%d, %d, '%s');", film_order.get_User_ID(), film_order.get_Film_ID(), date_for.format(film_order.get_Order_Date()));
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected void import_data(){
        try {
            String sql = "select * from FilmOrders";
            PreparedStatement p = connection.prepareStatement(sql);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                Integer User_ID = rs.getInt("User_ID");
                Integer Film_ID = rs.getInt("Film_ID");
                Date Order_Date = rs.getDate("Order_Date");
                filmorders_table.add(new FilmOrder(User_ID, Film_ID, Order_Date));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public Integer get_id_of_user(User user){
        try {
            String sql = String.format("""
                            SELECT "ID"\s
                            from users\s
                            Where "Name" = '%s' and "Surname" = '%s' and "Age" = %d;""",user.get_Name(), user.get_Surname(), user.get_Age());
            PreparedStatement p = connection.prepareStatement(sql);
            ResultSet rs = p.executeQuery();
            rs.next();
            Integer User_ID = rs.getInt("ID");
            return User_ID;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
    public void delete_film_order(FilmOrder film_order){
        filmorders_table.removeIf(fo -> (Objects.equals(fo.get_User_ID(),film_order.get_User_ID()))
                &&(Objects.equals(fo.get_Film_ID(), film_order.get_Film_ID())));

        try {
            String sql = String.format("""
                    DELETE FROM FilmOrders
                    Where "User_ID" = %d and "Film_ID" = %d and "Order_Date" = '%s';""", film_order.get_User_ID(), film_order.get_Film_ID(), date_for.format(film_order.get_Order_Date()));
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void change_user_id(FilmOrder film_order, int film_order_index, int new_user_ID){
        filmorders_table.get(film_order_index).set_User_ID(new_user_ID);
        try {
            String sql = String.format("""
                    UPDATE FilmOrders
                    SET "User_ID" = %d
                    Where "User_ID" = %d and "Film_ID" = %d and "Order_Date" = '%s';""", new_user_ID, film_order.get_User_ID(), film_order.get_Film_ID(), date_for.format(film_order.get_Order_Date()));
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void change_film_id(FilmOrder film_order, int film_order_index, int new_film_ID){
        filmorders_table.get(film_order_index).set_User_ID(new_film_ID);
        try {
            String sql = String.format("""
                    UPDATE FilmOrders
                    SET "Film_ID" = %d
                    Where "User_ID" = %d, "Film_ID" = %d, "Order_Date" = '%s';""", new_film_ID, film_order.get_User_ID(), film_order.get_Film_ID(), film_order.get_Order_Date());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void change_order_date(FilmOrder film_order, int film_order_index, Date new_order_date){
        filmorders_table.get(film_order_index).set_Order_Date(new_order_date);
        try {
            String sql = String.format("""
                    UPDATE FilmOrders
                    SET "Order_Date" = '%s'
                    Where "User_ID" = %d, "Film_ID" = %d, "Order_Date" = '%s';""", date_for.format(new_order_date), film_order.get_User_ID(), film_order.get_Film_ID(), date_for.format(film_order.get_Order_Date()));
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public Integer get_id_of_film(Film film){
        try {
            String sql = String.format("""
                            SELECT "ID"\s
                            from films\s
                            WHERE "Name" = '%s' and "Genre" = '%s' and "Premiere_Date" = '%s' and "Price" = %d;""",film.get_Name(), film.get_Genre(), date_for.format(film.get_Premiere_Date()), film.get_Price());
            PreparedStatement p = connection.prepareStatement(sql);
            ResultSet rs = p.executeQuery();
            rs.next();
            Integer Film_ID = rs.getInt("ID");
            return Film_ID;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
    public int get_max_User_ID_length() {
        if (filmorders_table.size() != 0)
        {
            return String.valueOf(filmorders_table.stream().max(Comparator.comparing(p -> String.valueOf(p.get_User_ID()).length())).get().get_User_ID()).length();
        }
        else {
            return 0;
        }
    }
    public int get_max_Film_ID_length() {
        if (filmorders_table.size() != 0)
        {
            return String.valueOf(filmorders_table.stream().max(Comparator.comparing(p -> String.valueOf(p.get_Film_ID()).length())).get().get_Film_ID()).length();
        }
        else {
            return 0;
        }
    }
}