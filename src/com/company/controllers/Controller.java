package com.company.controllers;

import com.company.databases.Database;
import com.company.Pair;
import com.company.datatypes.IntContainer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    protected boolean exit_status = false;
    protected static Scanner in = new Scanner(System.in);
    SimpleDateFormat date_for = new SimpleDateFormat("yyyy-MM-dd");

    protected static  <T> String get_options(ArrayList<Pair<String,T>> options_list)
    {
        String s = "";

        for (int i = 0; i < options_list.size(); i++) {
            s += (i + 1)+ ") " + options_list.get(i).get_first() + "\n";
        }
        return s;
    }
    protected boolean get_exit_status(){
        return exit_status;
    }
    public static void menu(ArrayList<Pair<String,Runnable>> list){

        String description = "Ниже перечислены опции которые вы можете выбрать.\n\n" +
                get_options(list) +
                "\nВпишите номер варианта который хотите выбрать.";
        System.out.flush();
        System.out.println(description);

        String input = in.nextLine();

        while (!check_list_input(input, list)) {
            input = in.nextLine();
        }
        list.get(Integer.parseInt(input) - 1).get_second().run();
    }
    protected static void display_message(String str){
        System.out.flush();
        System.out.println(str);
        in.nextLine();
    }
    protected static <T> boolean  check_list_input(String input, ArrayList<Pair<String,T>> options_list) {
        IntContainer i = new IntContainer(0);
        if (!try_parse_int(input, i))
        {
            System.out.println("Вы ввели не число!\nПопробуйте еще раз: ");
            return false;
        }
            else if (i.get_i() < 1 || i.get_i() > options_list.size())
        {
            System.out.println("Такого варианта нету в списке!\nПопробуйте еще раз: ");
            return false;
        }
        return true;
    }
    protected static boolean check_empty_input(String input){
        if (Objects.equals(input, ""))
        {
            System.out.println("Ввод не может быть пустым.\nПопробуйте еще раз:");
            return true;
        }
        return false;
    }
    protected static boolean check_date_format(String input){
        String valid_regex = "^\\d{4}-\\d{2}-\\d{2}$";
        Pattern valid_pattern = Pattern.compile(valid_regex);
        Matcher valid_matcher = valid_pattern.matcher(input);

        if (valid_matcher.matches()) {
            return true;
        }
        System.out.println("Дата не соответствует формату.\nПопробуйте еще раз:");
        return false;
    }
    protected static boolean check_format(String input) {
        String valid_regex = "^\s*[A-Za-zа-яА-Я0-9_-]+\s*$";
        Pattern valid_pattern = Pattern.compile(valid_regex);
        Matcher valid_matcher = valid_pattern.matcher(input);

        if (valid_matcher.matches()) {
            return true;
        }
        System.out.println("Слово не соответствует формату.\nПопробуйте еще раз:");
        return false;
    }
    protected static boolean try_parse_int(String value, IntContainer i) {
        try {
            Integer ie = Integer.parseInt(value);
            i.set_i(ie);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    protected static boolean check_int_parse(String input){
        if (check_empty_input(input)) return false;
        IntContainer i = new IntContainer(0);
        if (!try_parse_int(input, i))
        {
            System.out.println("Ваш ввод не соответствует формату десятичного числа!\nПопробуйте еще раз: ");
            return false;
        }
        if (i.get_i() < 0)
        {
            System.out.println("Число не может быть отрицательным!\nПопробуйте еще раз: ");
            return false;
        }
        return true;
    }
    protected void exit_menu(){
        exit_status = true;
    }
}
