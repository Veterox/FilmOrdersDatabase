package com.company.datatypes;

import java.sql.Date;
import java.text.DateFormat;

public class FilmOrder {
    private Integer User_ID;
    private Integer Film_ID;
    private Date Order_Date;

    public FilmOrder(Integer User_ID, Integer Film_ID, Date Order_Date){
        this.User_ID = User_ID;
        this.Film_ID = Film_ID;
        this.Order_Date = Order_Date;
    }
    public Integer get_User_ID(){
        return User_ID;
    }
    public void set_User_ID(Integer User_ID){
        this.User_ID = User_ID;
    }
    public Integer get_Film_ID(){
        return Film_ID;
    }
    public void set_Film_ID(Integer Film_ID){
        this.Film_ID = Film_ID;
    }
    public Date get_Order_Date(){
        return Order_Date;
    }
    public void set_Order_Date(Date Order_Date){
        this.Order_Date = Order_Date;
    }
}
