package com.company.datatypes;

public class IntContainer {
    private Integer i;
    public IntContainer(Integer i){
        this.i = i;
    }
    public Integer get_i(){
        return i;
    }
    public void set_i(Integer i){
        this.i = i;
    }
}
