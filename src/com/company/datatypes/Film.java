package com.company.datatypes;

import java.sql.Date;
import java.text.DateFormat;

public class Film {
    private String Name;
    private String Genre;
    private Date Premiere_Date;
    private Integer Price;

    public Film(String Name, String Genre, Date Premiere_Date, Integer Price){
        this.Name = Name;
        this.Genre = Genre;
        this.Premiere_Date = Premiere_Date;
        this.Price = Price;
    }
    public String get_Name(){
        return Name;
    }
    public void set_Name(String Name){
        this.Name = Name;
    }
    public String get_Genre(){
        return Genre;
    }
    public void set_Genre(String Genre){
        this.Genre = Genre;
    }
    public Date get_Premiere_Date(){
        return Premiere_Date;
    }
    public void set_Premiere_Date(Date Premiere_Date){
        this.Premiere_Date = Premiere_Date;
    }
    public Integer get_Price(){
        return Price;
    }
    public void set_Price(Integer Price){
        this.Price = Price;
    }
}
