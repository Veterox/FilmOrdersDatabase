package com.company.controllers;

import com.company.Pair;
import com.company.databases.FilmOrdersDatabase;
import com.company.datatypes.Film;
import com.company.datatypes.FilmOrder;
import com.company.datatypes.User;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.function.BiConsumer;

public class ControllerFilmOrder extends Controller {
    protected FilmOrdersDatabase d;
    public ControllerFilmOrder(FilmOrdersDatabase d) {
        this.d = d;
    }
    public boolean run(){
        ArrayList<Pair<String,Runnable>> options_list = new ArrayList<>();
        options_list.add(new Pair<>("Вывести базу данных заказов.", this::show_database));
        options_list.add(new Pair<>("Добавить заказ.", this::add_film_order));
        options_list.add(new Pair<>("Удалить заказ.", this::delete_film_order));
        options_list.add(new Pair<>("Изменить заказ.", this::change_film_order));
        options_list.add(new Pair<>("Вернуться к меню баз данных.", this::exit_menu));
        Controller.menu(options_list);
        return !exit_status;
    }
    protected static void change_menu(ArrayList<Pair<String, BiConsumer<FilmOrder, Integer>>> list, FilmOrder filmorder, Integer film_order_index){

        String description = "Ниже перечислены опции которые вы можете выбрать.\n\n" +
                get_options(list) +
                "\nВпишите номер варианта который хотите выбрать.";

        System.out.println(description);

        String input = in.nextLine();

        while (!check_list_input(input, list)) {
            input = in.nextLine();
        }
        list.get(Integer.parseInt(input) - 1).get_second().accept(filmorder, film_order_index);
    }

    protected void change_film_order(){


        System.out.println("Чтобы изменить заказ введите данные о пользователе заказывающем фильм: ");
        User user = ControllerUser.create_user_from_input();

        System.out.println("Чтобы изменить заказ введите данные о заказываемом фильме.\n");
        Film film = ControllerFilm.create_film_from_input();

        FilmOrder film_order = create_film_order_from_input(user, film);
        int film_order_index = d.index_of_film_order(film_order);
        while (film_order_index == -1) {
            System.out.println("Не удалось найти заказ по введенным данным.\n" +
                    "Попробуйте еще раз.");
            user = ControllerUser.create_user_from_input();
            film = ControllerFilm.create_film_from_input();
            film_order = create_film_order_from_input(user, film);
            film_order_index = d.index_of_film_order(film_order);
        }
        System.out.println("Заказ был найден.\n" +
                "Нужно выбрать елемент который вы бы хотели о нем поменять.");
        ArrayList<Pair<String, BiConsumer<FilmOrder, Integer>>> options_list = new ArrayList<>();
        options_list.add(new Pair<>("Изменить пользователя.", this::change_user_id));
        options_list.add(new Pair<>("Изменить фильм.", this::change_film_id));
        options_list.add(new Pair<>("Изменить дату заказа.", this::change_order_date));
        change_menu(options_list, film_order, film_order_index);
        display_message("Заказ был успешно изменен.\n" +
                "Чтобы вернуться нажмите Enter\n");
    }
    protected void change_user_id(FilmOrder film_order, Integer film_order_index){
        System.out.println("Введите данные нового пользователя.\n");
        User user = ControllerUser.create_user_from_input();
        while (d.get_id_of_user(user) == -1) {
            System.out.println("Не удалось найти данного пользователя.\n" +
                    "Попробуйте еще раз.");
            user = ControllerUser.create_user_from_input();
        }
        d.change_user_id(film_order, film_order_index, d.get_id_of_user(user));

    }
    protected void change_film_id(FilmOrder film_order, Integer film_order_index){
        System.out.println("Введите данные нового фильма.\n");
        Film film = ControllerFilm.create_film_from_input();
        while (d.get_id_of_film(film) == -1) {
            System.out.println("Не удалось найти данный фильм.\n" +
                    "Попробуйте еще раз.");
            film = ControllerFilm.create_film_from_input();
        }
        d.change_film_id(film_order, film_order_index, d.get_id_of_film(film));
    }
    protected void change_order_date(FilmOrder film_order, Integer film_order_index){
        System.out.println("Введите новую дату заказа (формат yyyy-mm-dd):\n" );
        String input = in.nextLine();
        while (!check_date_format(input))
        {
            input = in.nextLine();
        }
        Date date = new Date(new java.util.Date().getTime());
        try {
            date = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(input).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        d.change_order_date(film_order, film_order_index, date);
    }

    protected void delete_film_order(){


        System.out.println("Чтобы удалить заказ введите данные о пользователе заказывающем фильм.\n");
        User user = ControllerUser.create_user_from_input();

        System.out.println("Чтобы удалить заказ введите данные о заказываемом фильме.\n");
        Film film = ControllerFilm.create_film_from_input();

        FilmOrder film_order = create_film_order_from_input(user, film);
        int film_order_index = d.index_of_film_order(film_order);
        while (film_order_index == -1) {
            System.out.println("Не удалось найти заказ по введенным данным.\n" +
                    "Попробуйте еще раз.");
            user = ControllerUser.create_user_from_input();
            film = ControllerFilm.create_film_from_input();
            film_order = create_film_order_from_input(user, film);
            film_order_index = d.index_of_film_order(film_order);
        }
        d.delete_film_order(film_order);
        display_message("Заказ был успешно удален.\n" +
                "Чтобы вернуться нажмите Enter\n");
    }

    protected FilmOrder create_film_order_from_input(User user, Film film){
        Date date = new Date(System.currentTimeMillis());

        return new FilmOrder(d.get_id_of_user(user), d.get_id_of_film(film), date);


    }
    protected void add_film_order(){

        System.out.println("Чтобы добавить заказ введите данные о пользователе заказывающем фильм.\n");
        User user = ControllerUser.create_user_from_input();

        System.out.println("Чтобы добавить заказ введите данные о заказываемом фильме.\n");
        Film film = ControllerFilm.create_film_from_input();

        d.add_film_order(create_film_order_from_input(user, film));

        display_message("Заказ был успешно добавлен!\n" +
                "Чтобы вернуться нажмите Enter\n");
    }
    protected String write_table(){
        ArrayList<FilmOrder> data = d.get_data();
        String description = "";

        double tmp = Math.floor(Math.log10(data.size()));
        int id_l = (int) tmp;
        int d_ui_l = d.get_max_User_ID_length();
        int d_fi_l = d.get_max_Film_ID_length();

        String first_column = "#";
        String second_column = "ID Пользователя";
        String third_column = "ID Фильма";
        String forth_column = "Дата оформления заказа";


        String id = String.format("%-" + (Math.max(id_l, first_column.length()) + 1) + "s", first_column);
        String user_id = String.format("%-" + (Math.max(d_ui_l, second_column.length()) + 2) + "s", second_column);
        String film_id = String.format("%-" + (Math.max(d_fi_l, third_column.length()) + 2) + "s", third_column);
        String order_date = String.format("%-24s", forth_column);


        description += " " + id + "|  " + user_id + "|  " + film_id + "|  " + order_date + "\n";
        int i = 1;
        for(FilmOrder film_order: data)
        {
            id = String.format("%-" + (Math.max(id_l, first_column.length()) + 1) + "d", i++);
            user_id = String.format("%-" + (Math.max(d_ui_l, second_column.length()) + 2) + "d", film_order.get_User_ID());
            film_id = String.format("%-" + (Math.max(d_fi_l, third_column.length()) + 2) + "d", film_order.get_Film_ID());
            order_date = String.format("%-24s", date_for.format(film_order.get_Order_Date()));
            description += " " + id + "|  " + user_id + "|  " + film_id + "|  " + order_date + "\n";
        }

        return description;
    }
    protected void show_database(){

        String description  = "Таблица фильмов:\n" + write_table() + "Чтобы вернуться нажмите Enter\n";
        display_message(description);
    }

}
