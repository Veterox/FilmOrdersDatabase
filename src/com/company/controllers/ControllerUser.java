package com.company.controllers;
import com.company.Pair;
import com.company.databases.UsersDatabase;
import com.company.datatypes.User;

import java.util.ArrayList;
import java.util.function.BiConsumer;

public class ControllerUser extends Controller {
    protected UsersDatabase d;
    public ControllerUser(UsersDatabase d) {
        this.d = d;
    }
    public boolean run(){
        ArrayList<Pair<String,Runnable>> options_list = new ArrayList<>();
        options_list.add(new Pair<>("Вывести базу данных пользователей.", this::show_database));
        options_list.add(new Pair<>("Добавить пользователя.", this::add_user));
        options_list.add(new Pair<>("Удалить пользователя.", this::delete_user));
        options_list.add(new Pair<>("Изменить пользователя.", this::change_user));
        options_list.add(new Pair<>("Вернуться к меню баз данных.", this::exit_menu));
        Controller.menu(options_list);
        return !exit_status;
    }
    protected static void change_menu(ArrayList<Pair<String,BiConsumer<User, Integer>>> list, User user, Integer user_index){

        String description = "Ниже перечислены опции которые вы можете выбрать.\n\n" +
                get_options(list) +
                "\nВпишите номер варианта который хотите выбрать.";

        System.out.println(description);

        String input = in.nextLine();

        while (!check_list_input(input, list)) {
            input = in.nextLine();
        }
        list.get(Integer.parseInt(input) - 1).get_second().accept(user, user_index);
    }

    protected void change_user(){


        System.out.println("Чтобы изменить пользователя, введите данные о нем: ");
        User user = create_user_from_input();
        int user_index = d.index_of_user(user);
        while (user_index == -1) {
            System.out.println("Не удалось найти пользователя по введенным данным.\n" +
                    "Попробуйте еще раз.");
            user = create_user_from_input();
            user_index = d.index_of_user(user);
        }
        System.out.println("Пользователь был найден.\n" +
                "Нужно выбрать елемент который вы бы хотели о нем поменять.");
        ArrayList<Pair<String, BiConsumer<User, Integer>>> options_list = new ArrayList<>();
        options_list.add(new Pair<>("Изменить имя.", this::change_name));
        options_list.add(new Pair<>("Изменить фамилию.", this::change_surname));
        options_list.add(new Pair<>("Изменить возраст.", this::change_age));
        change_menu(options_list, user, user_index);
        display_message("Пользователь был успешно изменен.\n" +
                "Чтобы вернуться нажмите Enter\n");
    }
    protected void change_name(User user, Integer user_index){
        System.out.println("Введите новое имя пользователя.\n" +
                "(1 слово, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
        String input = in.nextLine();

        while (!check_format(input))
        {
            input = in.nextLine();
        }
        String Name = input;
        d.change_name(user, user_index, Name);

    }
    protected void change_surname(User user, Integer user_index){
        System.out.println("Введите новую фамилию пользователя.\n" +
                "(1 слово, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
        String input = in.nextLine();

        while (!check_format(input))
        {
            input = in.nextLine();
        }
        String Surname = input;
        d.change_surname(user, user_index, Surname);
    }
    protected void change_age(User user, Integer user_index){
        System.out.println("Введите новый возраст пользователя.\n" +
                "(1 слово, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
        String input = in.nextLine();
        while (!check_int_parse(input))
        {
            input = in.nextLine();
        }

        int Age = Integer.parseInt(input);
        d.change_age(user, user_index, Age);
    }
    protected void delete_user(){


        System.out.println("Чтобы удалить пользователя, введите данные о нем: ");
        User user = create_user_from_input();
        int user_index = d.index_of_user(user);
        while (user_index == -1) {
            System.out.println("Не удалось найти пользователя по введенным данным.\n" +
                    "Попробуйте еще раз.");
            user = create_user_from_input();
            user_index = d.index_of_user(user);
        }
        d.delete_user(user);
        display_message("Пользователь был успешно удален.\n" +
                "Чтобы вернуться нажмите Enter\n");
    }
    public static User create_user_from_input(){
        System.out.println("Введите Имя пользователя.\n" +
                "(1 слово, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
        String input = in.nextLine();

        while (!check_format(input))
        {
            input = in.nextLine();
        }
        String Name = input;


        System.out.println("Введите Фамилию пользователя.\n" +
                "(1 слово, минимум 1 символ, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -): ");
        input = in.nextLine();
        while (!check_format(input))
        {
            input = in.nextLine();
        }
        String Surname = input;


        System.out.println("Введите возраст пользователя: ");
        input = in.nextLine();
        while (!check_int_parse(input))
        {
            input = in.nextLine();
        }

        int Age = Integer.parseInt(input);

        return new User(Name, Surname, Age);

    }
    protected void add_user(){

        System.out.println("Чтобы добавить пользователя введите данные о нем.\n");

        d.add_user(create_user_from_input());

        display_message("Пользователь был успешно добавлен!\n" +
                "Чтобы вернуться нажмите Enter\n");
    }
    protected String write_table(){
        ArrayList<User> data = d.get_data();
        String description = "";

        double tmp = Math.floor(Math.log10(data.size()));
        int id_l = (int) tmp;
        int d_n_l = d.get_max_Name_length();
        int d_s_l = d.get_max_Surname_length();
        int d_a_l = d.get_max_Age_length();

        String first_column = "#";
        String second_column = "Имя";
        String third_column = "Фамилия";
        String forth_column = "Возраст";


        String id = String.format("%-" + (Math.max(id_l, first_column.length()) + 1) + "s", first_column);
        String name = String.format("%-" + (Math.max(d_n_l, second_column.length()) + 2) + "s", second_column);
        String surname = String.format("%-" + (Math.max(d_s_l, third_column.length()) + 2) + "s", third_column);
        String age = String.format("%-" + (Math.max(d_a_l, forth_column.length()) + 2) + "s", forth_column);

        description += " " + id + "|  " + name + "|  " + surname + "|  " + age + "\n";
        int i = 1;
        for(User user: data)
        {
            id = String.format("%-" + (Math.max(id_l, first_column.length()) + 1) + "d", i++);
            name = String.format("%-" + (Math.max(d_n_l, second_column.length()) + 2) + "s", user.get_Name());
            surname = String.format("%-" + (Math.max(d_s_l, third_column.length()) + 2) + "s", user.get_Surname());
            age = String.format("%-" + (Math.max(d_a_l, forth_column.length()) + 2) + "d", user.get_Age());
            description += " " + id + "|  " + name + "|  " + surname + "|  " + age + "\n";
        }

        return description;
    }
    protected void show_database(){

        String description  = "Таблица пользователей:\n" + write_table() + "Чтобы вернуться нажмите Enter\n";
        display_message(description);
    }

}
