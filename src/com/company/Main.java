package com.company;


import com.company.controllers.*;
import com.company.databases.*;
import com.company.datatypes.*;

import java.util.ArrayList;

public class Main {
    static boolean EXIT_PROGRAM = false;
    static UsersDatabase users_table = null;
    static FilmsDatabase films_table = null;
    static FilmOrdersDatabase film_orders_table = null;
    public static void main(String[] args) {

        while(!EXIT_PROGRAM){
            choose_database();
        }
        if(users_table != null && users_table.check_connected()){
            users_table.close_connection();
        }
        if(films_table != null && films_table.check_connected()){
            films_table.close_connection();
        }
        if(film_orders_table != null && film_orders_table.check_connected()){
            film_orders_table.close_connection();
        }
    }
    public static void choose_database(){
        ArrayList<Pair<String,Runnable>> options_list = new ArrayList<>();
        options_list.add(new Pair<>("Выбрать таблицу пользователей.", Main::open_users_database));
        options_list.add(new Pair<>("Выбрать таблицу фильмов.", Main::open_films_database));
        options_list.add(new Pair<>("Выбрать таблицу заказов.", Main::open_film_orders_database));
        options_list.add(new Pair<>("Закрыть программу.", Main::exit));
        Controller.menu(options_list);
    }
    public static void exit(){
        EXIT_PROGRAM = true;
    }
    public static void open_users_database(){
        if (users_table == null) {
            users_table = new UsersDatabase("Users");
        }
        ControllerUser c = new ControllerUser(users_table);

        while(c.run());
    }
    public static void open_films_database(){
        if (films_table == null) {
            films_table = new FilmsDatabase("Films");
        }
        ControllerFilm c = new ControllerFilm(films_table);
        while(c.run());
    }
    public static void open_film_orders_database(){
        if (users_table == null) {
            users_table = new UsersDatabase("Users");
        }
        if (films_table == null) {
            films_table = new FilmsDatabase("Films");
        }
        if (film_orders_table == null) {
            film_orders_table = new FilmOrdersDatabase("FilmOrders", users_table, films_table);
        }
        ControllerFilmOrder c = new ControllerFilmOrder(film_orders_table);
        while(c.run());
    }
}
