package com.company;

public class Pair<L,R> {
    private L first;
    private R second;
    public Pair(L first, R second){
        this.first = first;
        this.second = second;
    }
    public L get_first(){ return first; }
    public R get_second(){ return second; }
    public void setL(L first){ this.first = first; }
    public void setR(R second){ this.second = second; }
}
