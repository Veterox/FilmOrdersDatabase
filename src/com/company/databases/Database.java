package com.company.databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

public class Database{
    protected String jdbcURL = "jdbc:postgresql://localhost:5432/";

    protected String data_base_name = "postgres"; //вписать название базы данных
    protected String username = "postgres"; //вписать свое имя пользователя от postgreSQL
    protected String password = ""; //вписать свой пароль от postgreSQL

    protected boolean connected = false;
    protected Connection connection = null;
    protected String table_name;

    SimpleDateFormat date_for = new SimpleDateFormat("yyyy-MM-dd");
    public Database(String table_name){
        this.table_name = table_name;
        try {
            connection = DriverManager.getConnection(jdbcURL + data_base_name, username, password);
            connected = true;
            System.out.println("Connected to PostgreSQL server");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public boolean check_connected(){
        return connected;
    }
    public void close_connection(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}