package com.company.datatypes;

public class User {
    private String Name;
    private String Surname;
    private Integer Age;
    public User(String Name, String Surname, Integer Age){
        this.Name = Name;
        this.Surname = Surname;
        this.Age = Age;
    }
    public String get_Name(){
        return Name;
    }
    public void set_Name(String Name){
        this.Name = Name;
    }
    public String get_Surname(){
        return Surname;
    }
    public void set_Surname(String Surname){
        this.Surname = Surname;
    }
    public Integer get_Age(){
        return Age;
    }
    public void set_Age(Integer Age){
        this.Age = Age;
    }
}