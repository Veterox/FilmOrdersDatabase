package com.company.databases;

import com.company.datatypes.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class UsersDatabase extends Database{
    protected ArrayList<User> users_table;
    public UsersDatabase(String table_name) {
        super(table_name);
        users_table = new ArrayList<>();
        create_Users_table();
        import_data();
    }
    public int index_of_user(User user)
    {
        int user_index;
        OptionalInt optional_index = IntStream.range(0, users_table.size())
                .filter(i -> (Objects.equals(users_table.get(i).get_Name(), user.get_Name()))
                        &&(Objects.equals(users_table.get(i).get_Surname(), user.get_Surname()))
                        &&(Objects.equals(users_table.get(i).get_Age(), user.get_Age())))
                .findFirst();
        if(optional_index.isPresent()){
            user_index = optional_index.getAsInt();
            return user_index;
        }
        return -1;
    }
    public ArrayList<User> get_data(){
        return users_table;
    }

    public void change_name(User user, int user_index, String new_name){
        users_table.get(user_index).set_Name(new_name);
        try {
            String sql = String.format("""
                    UPDATE users
                    SET "Name" = '%s'
                    WHERE "Name" = '%s' and "Surname" = '%s' and "Age" = %d;""", new_name, user.get_Name(), user.get_Surname(), user.get_Age());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void change_surname(User user, int user_index, String new_surname){
        users_table.get(user_index).set_Surname(new_surname);
        try {
            String sql = String.format("""
                    UPDATE users
                    SET "Surname" = '%s'
                    WHERE "Name" = '%s' and "Surname" = '%s' and "Age" = %d;""", new_surname, user.get_Name(), user.get_Surname(), user.get_Age());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void change_age(User user, int user_index, int new_age){
        users_table.get(user_index).set_Age(new_age);
        try {
            String sql = String.format("""
                    UPDATE users
                    SET "Age" = %d
                    WHERE "Name" = '%s' and "Surname" = '%s' and "Age" = %d;""", new_age, user.get_Name(), user.get_Surname(), user.get_Age());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void add_user(User user){
        users_table.add(user);
        try {
            String sql = String.format("INSERT INTO users(\"Name\", \"Surname\", \"Age\")\n" +
                         "VALUES('%s','%s', %d);", user.get_Name(), user.get_Surname(), user.get_Age());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete_user(User user){
        users_table.removeIf(u -> (Objects.equals(u.get_Name(), user.get_Name()))
                &&(Objects.equals(u.get_Surname(), user.get_Surname()))
                &&(Objects.equals(u.get_Age(), user.get_Age())));

        try {
            String sql = String.format("DELETE FROM users\n" +
                    "WHERE \"Name\" = '%s' and \"Surname\" = '%s' and \"Age\" = %d;", user.get_Name(), user.get_Surname(), user.get_Age());;
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected void create_Users_table(){
        try {
            String sql = """
                    CREATE TABLE IF NOT EXISTS Users(
                    \t"ID" serial PRIMARY KEY,
                    \t"Name" VARCHAR ( 50 ) NOT NULL,
                    \t"Surname" VARCHAR ( 50 ) NOT NULL,
                    \t"Age" INT NOT NULL
                    );""";
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected void import_data(){
        try {
            String sql = "select * from Users";
            PreparedStatement p = connection.prepareStatement(sql);
            ResultSet rs = p.executeQuery();

            while (rs.next()) {
                String Name = rs.getString("Name");
                String Surname = rs.getString("Surname");
                int Age = rs.getInt("Age");
                users_table.add(new User(Name,Surname,Age));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public int get_max_Name_length() {
        if (users_table.size() != 0)
        {
            return users_table.stream().max(Comparator.comparing(p -> p.get_Name().length())).get().get_Name().length();
        }
        else {
            return 0;
        }
    }
    public int get_max_Surname_length() {
        if (users_table.size() != 0)
        {
            return users_table.stream().max(Comparator.comparing(p -> p.get_Surname().length())).get().get_Surname().length();
        }
        else {
            return 0;
        }
    }
    public int get_max_Age_length() {
        if (users_table.size() != 0)
        {
            return String.valueOf(users_table.stream().max(Comparator.comparing(p -> String.valueOf(p.get_Age()).length())).get().get_Age()).length();
        }
        else {
            return 0;
        }
    }
}
