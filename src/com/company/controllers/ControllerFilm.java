package com.company.controllers;

import com.company.Pair;
import com.company.databases.FilmsDatabase;
import com.company.datatypes.Film;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.function.BiConsumer;

public class ControllerFilm extends Controller {
    protected FilmsDatabase d;
    public ControllerFilm(FilmsDatabase d) {
        this.d = d;
    }
    public boolean run(){
        ArrayList<Pair<String,Runnable>> options_list = new ArrayList<>();
        options_list.add(new Pair<>("Вывести базу данных фильм.", this::show_database));
        options_list.add(new Pair<>("Добавить фильм.", this::add_film));
        options_list.add(new Pair<>("Удалить фильм.", this::delete_film));
        options_list.add(new Pair<>("Изменить фильм.", this::change_film));
        options_list.add(new Pair<>("Вернуться к меню баз данных.", this::exit_menu));
        Controller.menu(options_list);
        return !exit_status;
    }
    protected static void change_menu(ArrayList<Pair<String, BiConsumer<Film, Integer>>> list, Film film, Integer film_index){

        String description = "Ниже перечислены опции которые вы можете выбрать.\n\n" +
                get_options(list) +
                "\nВпишите номер варианта который хотите выбрать.";

        System.out.println(description);

        String input = in.nextLine();

        while (!check_list_input(input, list)) {
            input = in.nextLine();
        }
        list.get(Integer.parseInt(input) - 1).get_second().accept(film, film_index);
    }

    protected void change_film(){


        System.out.println("Чтобы изменить фильм, введите данные о нем: ");
        Film film = create_film_from_input();
        int film_index = d.index_of_film(film);
        while (film_index == -1) {
            System.out.println("Не удалось найти фильм по введенным данным.\n" +
                    "Попробуйте еще раз.");
            film = create_film_from_input();
            film_index = d.index_of_film(film);
        }
        System.out.println("Фильм был найден.\n" +
                "Нужно выбрать елемент который вы бы хотели о нем поменять.");
        ArrayList<Pair<String, BiConsumer<Film, Integer>>> options_list = new ArrayList<>();
        options_list.add(new Pair<>("Изменить название.", this::change_name));
        options_list.add(new Pair<>("Изменить жанр.", this::change_genre));
        options_list.add(new Pair<>("Изменить дату премьеры.", this::change_premiere_date));
        options_list.add(new Pair<>("Изменить цену.", this::change_price));
        change_menu(options_list, film, film_index);
        display_message("Фильм был успешно изменен.\n" +
                "Чтобы вернуться нажмите Enter\n");
    }
    protected void change_name(Film film, Integer film_index){
        System.out.println("Введите новое название фильма.\n" +
                "(1 слово, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
        String input = in.nextLine();

        while (!check_format(input))
        {
            input = in.nextLine();
        }
        String Name = input;
        d.change_name(film, film_index, Name);

    }
    protected void change_genre(Film film, Integer film_index){
        System.out.println("Введите новый жанр фильма.\n" +
                "(1 слово, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
        String input = in.nextLine();

        while (!check_format(input))
        {
            input = in.nextLine();
        }
        String Genre = input;
        d.change_genre(film, film_index, Genre);
    }
    protected void change_premiere_date(Film film, Integer film_index){
        System.out.println("Введите новую дату премьеры фильма (формат yyyy-mm-dd):\n" );
        String input = in.nextLine();
        while (!check_date_format(input))
        {
            input = in.nextLine();
        }
        Date date = new Date(new java.util.Date().getTime());
        try {
            date = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(input).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        d.change_premiere_date(film, film_index, date);
    }
    public void change_price(Film film, Integer film_index){
        System.out.println("Введите новую цену фильма.\n" +
                "(1 слово, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
        String input = in.nextLine();
        while (!check_int_parse(input))
        {
            input = in.nextLine();
        }

        int Price = Integer.parseInt(input);
        d.change_price(film, film_index, Price);
    }
    protected void delete_film(){


        System.out.println("Чтобы удалить фильм, введите данные о нем: ");
        Film film = create_film_from_input();
        int film_index = d.index_of_film(film);
        while (film_index == -1) {
            System.out.println("Не удалось найти фильм по введенным данным.\n" +
                    "Попробуйте еще раз.");
            film = create_film_from_input();
            film_index = d.index_of_film(film);
        }
        d.delete_film(film);
        display_message("Фильм был успешно удален.\n" +
                "Чтобы вернуться нажмите Enter\n");
    }
    public static Film create_film_from_input(){
        System.out.println("Введите название фильма.\n" +
                "(1 слово, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
        String input = in.nextLine();

        while (!check_format(input))
        {
            input = in.nextLine();
        }
        String Name = input;


        System.out.println("Введите жанр фильма.\n" +
                "(1 слово, минимум 1 символ, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -): ");
        input = in.nextLine();
        while (!check_format(input))
        {
            input = in.nextLine();
        }
        String Surname = input;


        System.out.println("Введите дату премьеры фильма (формат yyyy-mm-dd): ");
        input = in.nextLine();
        while (!check_date_format(input))
        {
            input = in.nextLine();
        }
        Date date = new Date(new java.util.Date().getTime());
        try {
            date = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(input).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("Введите цену фильма: ");
        input = in.nextLine();
        while (!check_int_parse(input))
        {
            input = in.nextLine();
        }

        int Price = Integer.parseInt(input);

        return new Film(Name, Surname, date, Price);


    }
    protected void add_film(){

        System.out.println("Чтобы добавить фильм введите данные о нем.\n");

        d.add_film(create_film_from_input());

        display_message("Фильм был успешно добавлен!\n" +
                "Чтобы вернуться нажмите Enter\n");
    }
    protected String write_table(){
        ArrayList<Film> data = d.get_data();
        String description = "";

        double tmp = Math.floor(Math.log10(data.size()));
        int id_l = (int) tmp;
        int d_n_l = d.get_max_Name_length();
        int d_g_l = d.get_max_Genre_length();
        int d_p_l = d.get_max_Price_length();

        String first_column = "#";
        String second_column = "Название";
        String third_column = "Жанр";
        String forth_column = "Дата премьеры";
        String fifth_column = "Цена";


        String id = String.format("%-" + (Math.max(id_l, first_column.length()) + 1) + "s", first_column);
        String name = String.format("%-" + (Math.max(d_n_l, second_column.length()) + 2) + "s", second_column);
        String genre = String.format("%-" + (Math.max(d_g_l, third_column.length()) + 2) + "s", third_column);
        String premiere_date = String.format("%-15s", forth_column);
        String price = String.format("%-" + (Math.max(d_p_l, fifth_column.length()) + 2) + "s", fifth_column);


        description += " " + id + "|  " + name + "|  " + genre + "|  " + premiere_date + "|  " + price + "\n";
        int i = 1;
        for(Film film: data)
        {
            id = String.format("%-" + (Math.max(id_l, first_column.length()) + 1) + "d", i++);
            name = String.format("%-" + (Math.max(d_n_l, second_column.length()) + 2) + "s", film.get_Name());
            genre = String.format("%-" + (Math.max(d_g_l, third_column.length()) + 2) + "s", film.get_Genre());
            premiere_date = String.format("%-15s", date_for.format(film.get_Premiere_Date()));
            price = String.format("%-" + (Math.max(d_p_l, fifth_column.length()) + 2) + "d", film.get_Price());
            description += " " + id + "|  " + name + "|  " + genre + "|  " + premiere_date + "|  " + price + "\n";
        }

        return description;
    }
    protected void show_database(){

        String description  = "Таблица фильмов:\n" + write_table() + "Чтобы вернуться нажмите Enter\n";
        display_message(description);
    }
}
