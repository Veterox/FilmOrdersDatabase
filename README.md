###About this program
This program is a console application with CRUD functionality for maintenance of 
film selling website.

It uses PostgreSQL database to maintain 3 databases: User, Films, FilmOrders.

Using menu (written in Russian) you can View all element or Add, Delete and 
Change any particular element of the table.

###Needed dependencies

You will need to:

1. Download folder `src` from this repository.
Put this folder into your empty IntelliJ IDEA project.
2. Download PostgreSQL SQL.(You can find how to install PostgreSQL here: 
https://phoenixnap.com/kb/install-postgresql-windows)
3. Download PostgreSQL JDBC driver (from here https://jdbc.postgresql.org/download.html) 
and put it into the project folder.
4. Put your server password into file `src\com\company\databases\Database` line 13.