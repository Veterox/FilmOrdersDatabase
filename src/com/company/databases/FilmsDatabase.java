package com.company.databases;

import com.company.datatypes.Film;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class FilmsDatabase extends Database{
    protected ArrayList<Film> films_table;
    public FilmsDatabase(String table_name) {
        super(table_name);
        films_table = new ArrayList<>();
        create_Films_table();
        import_data();
    }
    public int index_of_film(Film film)
    {
        int film_index;
        OptionalInt optional_index = IntStream.range(0, films_table.size())
                .filter(i -> (Objects.equals(films_table.get(i).get_Name(), film.get_Name()))
                        &&(Objects.equals(films_table.get(i).get_Genre(), film.get_Genre()))
                        &&(Objects.equals(films_table.get(i).get_Premiere_Date(), film.get_Premiere_Date()))
                        &&(Objects.equals(films_table.get(i).get_Price(), film.get_Price())))
                .findFirst();
        if(optional_index.isPresent()){
            film_index = optional_index.getAsInt();
            return film_index;
        }
        return -1;
    }
    public ArrayList<Film> get_data(){
        return films_table;
    }

    public void change_name(Film film, int film_index, String new_name){
        films_table.get(film_index).set_Name(new_name);
        try {
            String sql = String.format("""
                    UPDATE films
                    SET "Name" = '%s'
                    WHERE "Name" = '%s' and "Genre" = '%s' and "Premiere_Date" = '%s' and "Price" = %d;""", new_name, film.get_Name(), film.get_Genre(), date_for.format(film.get_Premiere_Date()), film.get_Price());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void change_genre(Film film, int film_index, String new_genre){
        films_table.get(film_index).set_Genre(new_genre);
        try {
            String sql = String.format("""
                    UPDATE films
                    SET "Genre" = '%s'
                    WHERE "Name" = '%s' and "Genre" = '%s' and "Premiere_Date" = '%s' and "Price" = %d;""", new_genre, film.get_Name(), film.get_Genre(), date_for.format(film.get_Premiere_Date()), film.get_Price());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void change_premiere_date(Film film, int film_index, Date new_premiere_date){
        films_table.get(film_index).set_Premiere_Date(new_premiere_date);
        try {
            String sql = String.format("""
                    UPDATE films
                    SET "Premiere_Date" = '%s'
                    WHERE "Name" = '%s' and "Genre" = '%s' and "Premiere_Date" = '%s' and "Price" = %d;""", new_premiere_date, film.get_Name(), film.get_Genre(), date_for.format(film.get_Premiere_Date()), film.get_Price());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void change_price(Film film, int film_index, int new_price){
        films_table.get(film_index).set_Price(new_price);
        try {
            String sql = String.format("""
                    UPDATE films
                    SET "Price" = %d
                    WHERE "Name" = '%s' and "Genre" = '%s' and "Premiere_Date" = '%s' and "Price" = %d;""", new_price, film.get_Name(), film.get_Genre(), date_for.format(film.get_Premiere_Date()), film.get_Price());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void add_film(Film film){
        films_table.add(film);
        try {
            String sql = String.format("INSERT INTO films(\"Name\", \"Genre\", \"Premiere_Date\", \"Price\")\n" +
                    "VALUES('%s','%s', '%s', %d);", film.get_Name(), film.get_Genre(), date_for.format(film.get_Premiere_Date()), film.get_Price());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete_film(Film film){
        films_table.removeIf(f -> (Objects.equals(f.get_Name(), film.get_Name()))
                &&(Objects.equals(f.get_Genre(), film.get_Genre()))
                &&(Objects.equals(f.get_Premiere_Date(), film.get_Premiere_Date()))
                &&(Objects.equals(f.get_Price(), film.get_Price())));

        try {
            String sql = String.format("DELETE FROM films\n" +
                    "WHERE \"Name\" = '%s' and \"Genre\" = '%s' and \"Premiere_Date\" = '%s' and \"Price\" = %d;", film.get_Name(), film.get_Genre(), date_for.format(film.get_Premiere_Date()), film.get_Price());
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected void create_Films_table(){
        try {
            String sql = """
                    CREATE TABLE IF NOT EXISTS  Films(
                    \t"ID" serial PRIMARY KEY,
                    \t"Name" VARCHAR ( 50 ) NOT NULL,
                    \t"Genre" VARCHAR ( 50 ) NOT NULL,
                    \t"Premiere_Date" DATE NOT NULL,
                    \t"Price" INT NOT NULL
                    );""";
            PreparedStatement p = connection.prepareStatement(sql);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected void import_data(){
        try {
            String sql = "select * from Films";
            PreparedStatement p = connection.prepareStatement(sql);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                String Name = rs.getString("Name");
                String Genre = rs.getString("Genre");
                Date Premiere_Date = rs.getDate("Premiere_Date");
                Integer Price = rs.getInt("Price");
                films_table.add(new Film(Name, Genre, Premiere_Date, Price));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public int get_max_Name_length() {
        if (films_table.size() != 0)
        {
            return films_table.stream().max(Comparator.comparing(p -> p.get_Name().length())).get().get_Name().length();
        }
        else {
            return 0;
        }
    }
    public int get_max_Genre_length() {
        if (films_table.size() != 0)
        {
            return films_table.stream().max(Comparator.comparing(p -> p.get_Genre().length())).get().get_Genre().length();
        }
        else {
            return 0;
        }
    }
    public int get_max_Price_length() {
        if (films_table.size() != 0)
        {
            return String.valueOf(films_table.stream().max(Comparator.comparing(p -> String.valueOf(p.get_Price()).length())).get().get_Price()).length();
        }
        else {
            return 0;
        }
    }
}
